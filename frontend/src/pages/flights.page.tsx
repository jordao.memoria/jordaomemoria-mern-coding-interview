import axios from "axios";
import { FlightCard } from "../components";
import { useFlights } from "../hooks";
import { FlightStatuses } from "../models";

export function FlightsPage() {
  const { data: flights } = useFlights();

  const statusList = ["Arrived", "Landing", "On Time", "Delayed", "Cancelled"];

  const updateStatus = async (flightId: string, current: FlightStatuses) => {
    let indexStatus = statusList.findIndex((value) => value === current);
    if (indexStatus === statusList.length) {
      indexStatus = 0;
    }
    const status = statusList[indexStatus];
    await axios.post("http://localhost:3001/v1/edit-status-flight", {
      id: flightId,
      status,
    });
  };

  return (
    <section>
      <h1>Scheduled Flights</h1>

      <div style={{ display: "flex", flexDirection: "column", gap: "0.5rem" }}>
        {flights?.map((flight) => (
          <FlightCard
            key={flight._id}
            origin={flight.origin}
            destination={flight.destination}
            code={flight.code}
            status={flight.status}
            onEdit={() => updateStatus(flight._id, flight.status)}
          />
        ))}
      </div>
    </section>
  );
}
