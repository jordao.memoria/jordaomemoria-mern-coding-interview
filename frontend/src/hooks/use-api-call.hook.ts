import { useEffect, useState } from "react";

type CallbackReturnType<CB> = CB extends () => Promise<infer T>
  ? T
  : CB extends () => infer T
  ? T
  : never;

export function useApiCall<CB extends () => Promise<any> | any>(
  apiCall: CB
): {
  data: CallbackReturnType<CB> | undefined;
  isLoading: boolean;
  error: Error | undefined;
} {
  const [data, setData] = useState<any>();
  const [isLoading, setIsLoading] = useState(true);
  const [error, setError] = useState<Error>();

  useEffect(() => {
    setIsLoading(true);

    Promise.resolve(apiCall())
      .then((data) => {
        console.log("Data: ", data);
        setData(data);
      })
      .catch(setError)
      .finally(() => setIsLoading(false));

    // Disabling exhaustive-deps as apiCall could be an inline function
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return { data, isLoading, error };
}
