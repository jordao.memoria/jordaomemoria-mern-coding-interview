import { Link } from "react-router-dom";

export function NavBar() {
  return (
    <div className="navbar">
      <Link to="/" style={{ height: "100%" }}>
        <img
          alt="BEON Tech"
          src="logo.webp"
          style={{ width: "auto", height: "100%" }}
        />
      </Link>

      <Link to="/flights">Flights</Link>
    </div>
  );
}
