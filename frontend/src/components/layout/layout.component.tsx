import { PropsWithChildren } from "react";
import { NavBar } from "./navbar";

export function Layout({ children }: PropsWithChildren<{}>) {
  return (
    <>
      <NavBar />
      <main>{children}</main>
    </>
  );
}
