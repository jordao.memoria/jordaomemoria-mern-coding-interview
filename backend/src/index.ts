import express from 'express'
import { flightsController } from './controllers'
import cors from 'cors'
import { db } from './memory-database'
import bodyParser from 'body-parser'

require('dotenv').config()

const port = process.env.PORT

const app = express()

app.use(cors({ origin: '*' }))
app.use(bodyParser.urlencoded({ extended: false }))
app.use(bodyParser.json())

const v1 = express.Router()
v1.use('/flights', flightsController)
v1.use('/edit-status-flight', flightsController)

app.use('/v1', v1)

// Connect to In-Memory DB

const loadDb = async () => {
    await db()
}

loadDb()

app.listen(port, () => {
    console.log(`[Live Coding Challenge] Running at http://localhost:${port}`)
})

export default app
