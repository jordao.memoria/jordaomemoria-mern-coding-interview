import express from 'express'
import { FlightsService } from '../services/flights.service'

const flightsService = new FlightsService()

export const flightsController = express.Router()

flightsController.get('/', (req, res, next) => {
    flightsService
        .getAll()
        .then((flights) => {
            res.status(200).send(flights)
        })
        .catch(next)
})

flightsController.post('/', (req, res, next) => {
    const { id, status } = req.body
    flightsService.updateFlightStatus(id, status).then(() => {
        console.log('Updated')
    })
})
