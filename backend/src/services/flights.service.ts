import { FlightsModel, FlightStatuses } from '../models/flights.model'

export class FlightsService {
    async getAll() {
        return FlightsModel.find()
    }
    async updateFlightStatus(id: string, status: FlightStatuses) {
        console.log(id, status)
        await FlightsModel.findByIdAndUpdate(id, { status })
        const f = await FlightsModel.findById(id)
        console.log('Flights: ', f)
    }
}
